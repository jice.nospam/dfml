# DFML - a developer friendly modeling language

A `XML`/`json`/`YAML` replacement for external data configuration with a focus on human readability and ease of use for the developer.

## Example

### config.dfml
    Room {
        name="01"
        entities=[
        Entity {
            name="elf"
            components=[
                VoxelObject{ name="elf.vox" },
                CreatureControl {},
                PlayerInput {},
                SoundEmitter {},
            ]
        },
        Entity {
            name="room"
            components=[
                PngLevel { name="room"}
            ]
        },
        Entity {
            name="cam"
            components=[
                Camera {fov = 40.0},
                Vector3 {
                    transfo = [
                        Tilt { angle=55.0 },
                        Translate {x=0.0 y=-5.0 z=120.0 }
                    ]
                },
                RailTracker {
                    rails=[
                        Rail {
                            start_pos = Vector3 {x=-55.0 y=-100.0 z=64.0 }
                            end_pos = Vector3 {x=60.0 y=-100.0 z=64.0}
                            target_name="elf"
                            axis="x"
                            offset=0.0
                        },
                        Rail {
                            start_pos = Vector3 {x=0.0 y=-120.0 z=64.0 }
                            end_pos = Vector3 {x=0.0 y=-80.0 z=64.0}
                            target_name="elf"
                            axis="y"
                            offset=-2.0
                        }
                   ]
                }
            ]
        },
        Entity {
            name="sun"
            components=[
                Light {
                   direction = Vector3 {x=1.0 y=1.0 z=1.0}
                }
            ]
        },
        Entity {
            name="exit"
            components=[
                BoundingBox {
                    min = Vector3 {x=112.0 y=-4.0 z=0.0}
                    max = Vector3 {x=120.0 y=3.0 z=14.0}
                },
                CollisionDetector {
                    message = CollMessage {txt="detected!"}
                }
            ]
        }
    ]}

### config.dfmt
    struct Tilt {
        angle: float
    }
    struct Translate : Vector3
    struct VecTransfo : Tilt|Translate
    struct Vector3 {
        x:float
        y:float
        z:float
        transfo?:VecTransfo[]
    }
    struct BoundingBox {
        min:Vector3
        max:Vector3
    }
    struct CollMessage {
        txt:string
    }
    struct CollisionDetector {
        message?:CollMessage
    }
    struct Light {
        direction: Vector3
    }
    struct Rail {
        start_pos: Vector3
        end_pos: Vector3
        target_name: string
        axis: "x"|"y"|"z"
        offset: float
    }
    struct RailTracker {
        rails=Rail[]
    }
    struct Camera {
        fov: float
    }
    struct Component : Camera|RailTracker|Light|CollisionDetector|BoundingBox
    struct Entity {
        name: string
        components: Component[]
    }
    struct Room {
        name: string
        entities: Entity[]
    }

## Grammar
    value ::= bool | int | float | char | string | array | struct
    bool ::= "true" | "false"
    int ::= <decimal (123) or hexadecimal (0x12f) notation>
    float ::= <standard (3.14) or scientific (1.2e-4) notation>
    char ::= <standard ('a'), unicode ('\u2764') or hexadecimal ('\x40') notation>
    string ::= "blabla"
    value_list = value | value "," value_list
    array ::= "[" value_list "]"
    struct ::= identifier "{" field_list "}"
    field_list ::= field_decl | field_decl field_list
    field_decl ::= identifier "=" value
    identifier ::= [_a-zA-Z][_a-zA-Z0-9]?

## API
### Parser

```
use dfml::{Parser,Token};

let parser = Parser::new("field = 3");
assert_eq!(
    parser.collect::<Vec<Token>>(),
    vec![
        Token::Property("field"),Token::Integer(3)
    ]);
```
