extern crate dfml;

use std::fs::File;
use std::io::Read;
use dfml::{Parser, Token};

#[test]
fn read_file() {
    let mut file = File::open("tests/test.dfml").expect("error");
    let mut buf = String::new();
    file.read_to_string(&mut buf).expect("error");
    let parser = Parser::new(&buf);
    assert_eq!(
        parser.collect::<Vec<Token>>(),
        vec![
            Token::Property("conf"),
            Token::BeginStruct("Config"), Token::Property("video"), Token::BeginStruct("Video"),
            Token::Property("width"), Token::Integer(1280),
            Token::Property("height"), Token::Integer(960),
            Token::Property("fullscreen"), Token::Boolean(true),
            Token::Property("gamma"), Token::Float(0.7),
            Token::Property("boss_key"), Token::Char('q'),
            Token::Property("title"), Token::Str("Welcome"),
            Token::Property("fps"), Token::BeginArray,
            Token::Integer(30),
            Token::Integer(60),
            Token::EndArray,
            Token::EndStruct, Token::EndStruct,
        ]
    );
}

#[test]
fn read_big_file() {
    let mut file = File::open("tests/big.dfml").expect("error");
    let mut buf = String::new();
    file.read_to_string(&mut buf).expect("error");
    let parser = Parser::new(&buf);
    println!("{:?}", parser.collect::<Vec<Token>>());
}
