use std::any::Any;

use super::{Registry,ToFactory,Parser,Token};

pub struct Factory {
    reg: Registry,
    blocks: Vec<BlockObject>,
}

enum BlockObject {
    ARRAY(Vec<Box<Any>>),
    STRUCT(Box<Any>),
}

pub trait StructFactory {
    fn new_struct(&mut self, name: &str) -> Box<Any>;
}

impl Factory {
    pub fn new() -> Factory {
        Factory {
            reg: Registry::new(),
            blocks: Vec::new(),
        }
    }
    pub fn register_struct<T:ToFactory<T> + 'static>(&mut self, name: &str, value: T) {
        self.reg.register(name, value);
    }
    pub fn build(&mut self,dfml_content: &str) {
        let parser = Parser::new(dfml_content);
        for tok in parser {
            self.handle_token(tok);
        }
    }
    fn handle_token(&mut self, tok: Token) {
        match tok {
            Token::Property(id) => (),
            Token::BeginStruct(name) => self.new_struct(name),
            Token::BeginArray => self.blocks.push(BlockObject::ARRAY(Vec::new()),
            _ => (),
        }
    }
    fn new_struct(&mut self, name: &str) {
        let instance = match str {

        }
        self.blocks.push(BlockObject::STRUCT(Box::new(self.reg.new_instance(name))))
    }
}