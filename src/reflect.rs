use std::any::Any;
use std::collections::HashMap;

pub struct Registry {
    factories: HashMap<String, Box<Any>>,
}

impl Registry {
    pub fn new() -> Registry {
        Registry {
            factories: HashMap::new(),
        }
    }

    pub fn register<T: ToFactory<T> + 'static>(&mut self, name: &str, value: T) {
        self.factories.insert(
            name.to_string(),
            Box::new(value.to_factory()) as Box<Any>
        );
    }

    pub fn new_instance<T: 'static>(&self, name: &str) -> T {
        let item = self.factories.get(name).unwrap();
        let factory = item.downcast_ref::<Factory<T>>().unwrap();
        factory.take()
    }
}

pub struct Factory<'a, T> {
    getter: Box<Getter<T> + 'a>,
}

impl<'a, T> Factory<'a, T> {
    fn take(&self) -> T {
        self.getter.take()
    }
}

pub trait ToFactory<T> {
    fn to_factory<'r>(self) -> Factory<'r, T>;
}

impl<T: Any+Clone> ToFactory<T> for T {
    fn to_factory<'r>(self) -> Factory<'r, T> {
        Factory { getter: Box::new(self.clone()) }
    }
}


impl<T:Any+Clone> Getter<T> for T {
    fn take(&self) -> T {
        self.clone()
    }
}

trait Getter<T> {
    fn take(&self) -> T;
}

pub trait Reflector<'a> {
    fn set_field(&mut self, name: &'a str, value: Box<Any>);
}

macro_rules! reflect {
    ($s: ty, $( ($x:ident, $n:expr, $t:ty) ),*) => {
        impl<'a> Reflector<'a> for $s {
            fn set_field(&mut self, name: &'a str, value: Box<Any>) {
                match name {
                    $(
                    $n => self.$x=*value.downcast::<$t>().unwrap(),
                    )*
                    _ => ()
                }
            }
        }
    };
}

#[cfg(test)]
mod tests {
    use std::any::Any;
    use super::{Registry,Reflector};


    #[derive(Debug,Clone,PartialEq)]
    struct MyStruct {
        intfield: u32,
        boolfield: bool,
    }
    reflect!(MyStruct, (intfield, "intfield", u32),(boolfield, "boolfield", bool));

    #[test]
    fn main() {
        let mut registry = Registry::new();

        registry.register("MyStruct", MyStruct{intfield:0,boolfield:false});
        let mut v: MyStruct = registry.new_instance("MyStruct");

        assert_eq!(v, MyStruct{intfield:0,boolfield:false});

        v.set_field("intfield",Box::new(30u32));
        v.set_field("boolfield",Box::new(true));
        assert_eq!(v, MyStruct{intfield:30,boolfield:true});

    }
}
