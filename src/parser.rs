use std::fmt;

#[derive(PartialEq, Clone, Debug)]
enum Block {
    STRUCT,
    ARRAY,
}

#[derive(Debug, PartialEq)]
pub enum Token<'a> {
    Boolean(bool),
    Integer(u32),
    Float(f32),
    Char(char),
    Str(&'a str),
    BeginArray,
    EndArray,
    BeginStruct(&'a str),
    EndStruct,
    Property(&'a str),
    Error(ParseError<'a>),
}

#[derive(PartialEq)]
pub enum ParseError<'a> {
    Expected(usize, &'a str),
    MissingIdentifier(usize),
    BadIntegerFormat(usize),
    BadBoolFormat(usize),
    BadFloatFormat(usize),
    BadCharFormat(usize),
    BadStringFormat(usize),
    BadArrayFormat(usize),
    BadStructFormat(usize),
    UnknownToken(usize),
    Eof,
}

impl<'a> fmt::Debug for ParseError<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParseError::Expected(line, val) => {
                write!(f, "Error line {} : expected '{}'", line, val)
            }
            ParseError::MissingIdentifier(line) => {
                write!(f, "Error line {} : missing identifier", line)
            }
            ParseError::BadIntegerFormat(line) => {
                write!(f, "Error line {} : bad integer format", line)
            }
            ParseError::BadBoolFormat(line) => {
                write!(f, "Error line {} : bad boolean format", line)
            }
            ParseError::BadFloatFormat(line) => write!(f, "Error line {} : bad float format", line),
            ParseError::BadCharFormat(line) => write!(f, "Error line {} : bad char format", line),
            ParseError::BadStringFormat(line) => {
                write!(f, "Error line {} : bad string format", line)
            }
            ParseError::BadArrayFormat(line) => write!(f, "Error line {} : bad array format", line),
            ParseError::BadStructFormat(line) => {
                write!(f, "Error line {} : bad struct format", line)
            }
            ParseError::UnknownToken(line) => write!(f, "Error line {} : unknown token", line),
            ParseError::Eof => write!(f, "Unexpected end of file"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ParserState {
    idx: usize,
    line: usize,
    old_idx: usize,
    old_line: usize,
    value_expected: bool,
    block_stack: Vec<Block>,
    array_begun: bool,
}

pub struct Parser<'a> {
    content: &'a str,
    state: ParserState,
    backup_stack: Vec<ParserState>,
}

impl<'a> Iterator for Parser<'a> {
    type Item = Token<'a>;
    /// Iterates over the tokens in a DFML string.
    ///
    /// # Examples
    ///
    /// ```
    /// use dfml::{Parser,Token};
    ///
    /// let parser = Parser::new("field = 3");
    /// assert_eq!(parser.collect::<Vec<Token>>(), vec![Token::Property("field"),Token::Integer(3)]);
    /// ```
    fn next(&mut self) -> Option<Token<'a>> {
        if !self.state.value_expected {
            let token = self.parse_property();
            self.state.value_expected = token != Some(Token::EndStruct) || self.is_in_array();
            // println!("{:?}",token);
            token
        } else {
            if self.is_in_array() && !self.state.array_begun {
                let _ = self.expect(",");
            }
            let token = self.parse_value();
            self.state.value_expected = self.is_in_array();
            // println!("{:?}",token);
            Some(token)
        }
    }
}

impl<'a> Parser<'a> {
    pub fn new(content: &str) -> Parser {
        Parser {
            content,
            state: ParserState {
                idx: 0,
                line: 1,
                old_idx: 0,
                old_line: 1,
                value_expected: false,
                block_stack: Vec::new(),
                array_begun: false,
            },
            backup_stack: Vec::new(),
        }
    }
    fn is_in_array(&self) -> bool {
        !self.state.block_stack.is_empty() && self.state.block_stack.last() == Some(&Block::ARRAY)
    }
    pub fn get_current_line(&self) -> usize {
        self.state.line
    }
    fn parse_property(&mut self) -> Option<Token<'a>> {
        self.skip_spaces();
        if self.state.idx == self.content.len() {
            None
        } else if let Ok(id) = self.parse_identifier() {
            if self.expect("=").is_ok() {
                Some(Token::Property(id))
            } else {
                self.state.idx = self.content.len();
                Some(Token::Error(ParseError::Expected(self.state.line, "=")))
            }
        } else if self.expect("}").is_ok() {
            self.state.block_stack.pop();
            Some(Token::EndStruct)
        } else {
            self.state.idx = self.content.len();
            Some(Token::Error(ParseError::MissingIdentifier(self.state.line)))
        }
    }
    fn parse_value(&mut self) -> Token<'a> {
        self.state.array_begun = false;
        if let Ok(value) = self.parse_float() {
            Token::Float(value)
        } else if let Ok(value) = self.parse_integer() {
            Token::Integer(value)
        } else if let Ok(value) = self.parse_bool() {
            Token::Boolean(value)
        } else if let Ok(value) = self.parse_char() {
            Token::Char(value)
        } else if let Ok(value) = self.parse_string() {
            Token::Str(value)
        } else if self.parse_array().is_ok() {
            self.state.block_stack.push(Block::ARRAY);
            self.state.array_begun = true;
            Token::BeginArray
        } else if self.expect("]").is_ok() {
            self.state.block_stack.pop();
            Token::EndArray
        } else {
            let token = self.parse_struct();
            if let Token::Error(e) = token {
                self.state.idx = self.content.len();
                return Token::Error(e);
            }
            self.state.block_stack.push(Block::STRUCT);
            token
        }
    }
    fn backup(&mut self) {
        let state = self.state.clone();
        // println!("backup {:?}",state);
        self.backup_stack.push(state);
    }
    fn restore(&mut self) {
        self.state = self.backup_stack.pop().unwrap();
        // println!("restore {:?}",self.state);
    }
    fn parse_struct(&mut self) -> Token<'a> {
        self.backup();
        self.skip_spaces();
        if let Ok(id) = self.parse_identifier() {
            self.expect("{").unwrap();
            self.backup_stack.pop();
            return Token::BeginStruct(id);
        }
        self.restore();
        Token::Error(ParseError::BadStructFormat(self.state.line))
    }
    fn parse_identifier(&mut self) -> Result<&'a str, ParseError> {
        self.backup();
        self.skip_spaces();
        let mut iter = self.content[self.state.idx..].chars();
        if !is_identifier_first_char(iter.next()) {
            self.restore();
            return Err(ParseError::MissingIdentifier(self.state.line));
        }
        let start = self.state.idx;
        let mut end: usize = self.state.idx + 1;
        while end < self.content.len() && is_identifier_char(iter.next()) {
            end += 1;
        }
        self.state.idx = end;
        self.backup_stack.pop();
        Ok(&self.content[start..end])
    }
    fn parse_array(&mut self) -> Result<(), ParseError> {
        self.backup();
        self.skip_spaces();
        if self.expect("[").is_ok() {
            self.backup_stack.pop();
            return Ok(());
        }
        self.restore();
        Err(ParseError::BadArrayFormat(self.state.line))
    }
    fn parse_integer(&mut self) -> Result<u32, ParseError> {
        self.backup();
        self.skip_spaces();
        if self.expect("0x").is_ok() || self.expect("0X").is_ok() {
            if let Ok(value) = self.get_token(|c| !is_token_end(c)) {
                return u32::from_str_radix(value, 16).map_err(|_| {
                    self.restore();
                    ParseError::BadIntegerFormat(self.state.line)
                });
            }
        } else if let Ok(value) = self.get_token(|c| !is_token_end(c)) {
            return u32::from_str_radix(value, 10).map_err(|_| {
                self.restore();
                ParseError::BadIntegerFormat(self.state.line)
            });
        }
        self.restore();
        Err(ParseError::BadIntegerFormat(self.state.line))
    }
    fn parse_char(&mut self) -> Result<char, ParseError> {
        self.backup();
        self.skip_spaces();
        if self.expect("'").is_ok() {
            if let Ok(value) = self.get_token(|c| c != '\'') {
                let res = value
                    .parse::<char>()
                    .map_err(|_| ParseError::BadCharFormat(self.state.line));
                if self.expect("'").is_ok() {
                    self.backup_stack.pop();
                    return res;
                }
            }
        }
        self.restore();
        Err(ParseError::BadCharFormat(self.state.line))
    }
    fn parse_string(&mut self) -> Result<&'a str, ParseError> {
        self.backup();
        self.skip_spaces();
        if self.expect("\"").is_ok() {
            if let Ok(value) = self.get_token(|c| c != '\"') {
                if self.expect("\"").is_ok() {
                    self.backup_stack.pop();
                    return Ok(value);
                }
            }
        }
        self.restore();
        Err(ParseError::BadStringFormat(self.state.line))
    }
    fn parse_float(&mut self) -> Result<f32, ParseError> {
        self.backup();
        self.skip_spaces();
        if let Ok(value) = self.get_token(|c| !is_token_end(c)) {
            if value.contains('.') {
                return value.parse::<f32>().map_err(|_| {
                    self.restore();
                    ParseError::BadFloatFormat(self.state.line)
                });
            }
        }
        self.restore();
        Err(ParseError::BadFloatFormat(self.state.line))
    }
    fn parse_bool(&mut self) -> Result<bool, ParseError> {
        self.backup();
        self.skip_spaces();
        if self.expect("true").is_ok() {
            self.backup_stack.pop();
            return Ok(true);
        } else if self.expect("false").is_ok() {
            self.backup_stack.pop();
            return Ok(false);
        }
        self.restore();
        Err(ParseError::BadBoolFormat(self.state.line))
    }
    fn expect(&mut self, value: &'static str) -> Result<(), ParseError> {
        self.backup();
        self.skip_spaces();
        if self.content[self.state.idx..].starts_with(value) {
            self.state.idx += value.len();
            self.backup_stack.pop();
            return Ok(());
        }
        self.restore();
        Err(ParseError::Expected(self.state.line, value))
    }
    fn skip_spaces(&mut self) {
        let _ = self.get_token(char::is_whitespace);
    }
    fn get_token<F>(&mut self, f: F) -> Result<&'a str, ()>
    where
        F: Fn(char) -> bool,
    {
        let iter = self.content[self.state.idx..].chars();
        let start = self.state.idx;
        for c in iter {
            if c == '\n' {
                self.state.line += 1;
            }
            if f(c) {
                self.state.idx += 1;
            } else {
                break;
            }
        }
        if self.state.idx > start {
            Ok(&self.content[start..self.state.idx])
        } else {
            Err(())
        }
    }
    pub fn expect_array_property(&mut self, expected_name: &str) -> bool {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                if let Some(Token::BeginArray) = self.next() {
                    self.backup_stack.pop();
                    return true;
                }
            }
        }
        self.restore();
        false
    }
    pub fn expect_int_array(&mut self, expected_name: &str) -> Option<Vec<u32>> {
        self.backup();
        if self.expect_array_property(expected_name) {
            if let Some(mut val) = self.next() {
                let mut ret = Vec::new();
                while val != Token::EndArray {
                    if let Token::Integer(i) = val {
                        ret.push(i);
                    }
                    val = self.next().unwrap();
                }
                self.backup_stack.pop();
                return Some(ret);
            }
        }
        self.restore();
        None
    }
    pub fn expect_int_property(&mut self, expected_name: &str) -> Option<u32> {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                if let Some(Token::Integer(i)) = self.next() {
                    self.backup_stack.pop();
                    return Some(i);
                }
            }
        }
        self.restore();
        None
    }
    pub fn expect_bool_property(&mut self, expected_name: &str) -> Option<bool> {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                if let Some(Token::Boolean(i)) = self.next() {
                    self.backup_stack.pop();
                    return Some(i);
                }
            }
        }
        self.restore();
        None
    }
    pub fn expect_string_property(&mut self, expected_name: &str) -> Option<String> {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                if let Some(Token::Str(s)) = self.next() {
                    self.backup_stack.pop();
                    return Some(s.to_string());
                }
            }
        }
        self.restore();
        None
    }
    pub fn expect_float_property(&mut self, expected_name: &str) -> Option<f32> {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                if let Some(Token::Float(f)) = self.next() {
                    self.backup_stack.pop();
                    return Some(f);
                }
            }
        }
        self.restore();
        None
    }
    pub fn expect_property(&mut self, expected_name: &str) -> bool {
        self.backup();
        if let Some(Token::Property(name)) = self.next() {
            if name == expected_name {
                return true;
            }
        }
        self.restore();
        false
    }
}

fn is_token_end(c: char) -> bool {
    c.is_whitespace() || c == ',' || c == ']' || c == '}'
}
fn is_identifier_first_char(c: Option<char>) -> bool {
    if let Some(c) = c {
        c == '_' || c.is_alphabetic()
    } else {
        false
    }
}
fn is_identifier_char(c: Option<char>) -> bool {
    if let Some(c) = c {
        c == '_' || c.is_alphanumeric()
    } else {
        false
    }
}

#[cfg(test)]
mod tests {
    use super::{Parser, Token};
    #[test]
    fn parse_bool_true() {
        let p = Parser::new("prop=true");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Boolean(true)]
        );
    }
    #[test]
    fn parse_bool_false() {
        let p = Parser::new("prop=false");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Boolean(false)]
        );
    }
    #[test]
    fn parse_int_10() {
        let p = Parser::new("prop=320");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Integer(320)]
        );
    }
    #[test]
    fn parse_int_16() {
        let p = Parser::new("prop=0x320");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Integer(0x320)]
        );
    }
    #[test]
    fn parse_float_std() {
        let p = Parser::new("prop=0.5");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Float(0.5)]
        );
    }
    #[test]
    fn parse_float_sci() {
        let p = Parser::new("prop=1.0e-1");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Float(1.0e-1)]
        );
    }
    #[test]
    fn parse_char_ascii() {
        let p = Parser::new("prop='a'");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Char('a')]
        );
    }
    // TODO FIXME. see https://github.com/rust-lang/rust/issues/44883
    #[ignore]
    #[test]
    fn parse_char_unicode() {
        let p = Parser::new("prop='\\u2764'");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Char('\u{2764}')]
        );
    }
    // TODO FIXME. see https://github.com/rust-lang/rust/issues/44883
    #[ignore]
    #[test]
    fn parse_char_hexa() {
        let p = Parser::new("prop='\\x40'");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Char('\x40')]
        );
    }
    #[test]
    fn parse_string() {
        let p = Parser::new("prop=\"string\"");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![Token::Property("prop"), Token::Str("string")]
        );
    }
    #[test]
    fn parse_array_bool() {
        let p = Parser::new("prop=[false,true]");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginArray,
                Token::Boolean(false),
                Token::Boolean(true),
                Token::EndArray
            ]
        );
    }
    #[test]
    fn parse_array_int() {
        let p = Parser::new("prop=[0,2]");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginArray,
                Token::Integer(0),
                Token::Integer(2),
                Token::EndArray
            ]
        );
    }
    #[test]
    fn parse_array_float() {
        let p = Parser::new("prop=[0.0,2.0]");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginArray,
                Token::Float(0.0),
                Token::Float(2.0),
                Token::EndArray
            ]
        );
    }
    #[test]
    fn parse_array_char() {
        let p = Parser::new("prop=['a','e']");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginArray,
                Token::Char('a'),
                Token::Char('e'),
                Token::EndArray
            ]
        );
    }
    #[test]
    fn parse_array_string() {
        let p = Parser::new("prop=[\"a\",\"e\"]");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginArray,
                Token::Str("a"),
                Token::Str("e"),
                Token::EndArray
            ]
        );
    }
    #[test]
    fn parse_struct() {
        let p = Parser::new("prop=MyStruct{}");
        assert_eq!(
            p.collect::<Vec<Token>>(),
            vec![
                Token::Property("prop"),
                Token::BeginStruct("MyStruct"),
                Token::EndStruct
            ]
        );
    }
    #[test]
    fn expect_int_prop() {
        let mut p = Parser::new("iprop=5");
        assert_eq!(p.expect_int_property("badpropname"), None);
        assert_eq!(p.expect_int_property("iprop"), Some(5));
    }
    #[test]
    fn expect_bool_prop() {
        let mut p = Parser::new("bprop=5");
        assert_eq!(p.expect_bool_property("bprop"), None);
        p = Parser::new("bprop=true");
        assert_eq!(p.expect_bool_property("bprop"), Some(true));
    }
    #[test]
    fn expect_float_prop() {
        let mut p = Parser::new("fprop=5.0");
        assert_eq!(p.expect_float_property("badpropname"), None);
        assert_eq!(p.expect_float_property("fprop"), Some(5.0));
    }
    #[test]
    fn expect_array_property() {
        let mut p = Parser::new("fprop=[5.0,1.0]");
        assert_eq!(p.expect_array_property("badpropname"), false);
        assert_eq!(p.expect_array_property("fprop"), true);
    }
    #[test]
    fn expect_int_array() {
        let mut p = Parser::new("iprop=[5,1]");
        assert_eq!(p.expect_int_property("badpropname"), None);
        assert_eq!(p.expect_int_array("iprop"), Some(vec![5, 1]));
    }
    #[test]
    fn expect_string_property() {
        let mut p = Parser::new("sprop=\"test\"");
        assert_eq!(p.expect_string_property("badpropname"), None);
        assert_eq!(p.expect_string_property("sprop"), Some("test".to_string()));
    }
}
